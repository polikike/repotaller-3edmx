var clientesObtenidos;

function gestionClientes() {
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest();  //para hacer la consulta 
    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //validando que termine  de cargar tiene difernetes estados 1 preparando 2 enviando 3 esperando respues 4 respondida
            console.table(JSON.parse(request.responseText).value);  //btener el value
            clientesObtenidos = request.responseText;
            procesarClientes();
        }
    }

    request.open("GET", url, true);
    request.send();
}


/*var clientesObtenidos;

function getProductos() {
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
    var request = new XMLHttpRequest();  //para hacer la consulta 
    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //validando que termine  de cargar tiene difernetes estados 1 preparando 2 enviando 3 esperando respues 4 respondida
            //console.table(JSON.parse(request.responseText).value);  //btener el value
            clientesObtenidos = request.responseText;
            procesarClientes();
        }
    }

    request.open("GET", url, true);
    request.send();
}*/

function procesarClientes() {
    var JSONClientes = JSON.parse(clientesObtenidos);
//    alert(JSONClientes.value[0].ProductName);
    var divTabla = document.getElementById("divClientes");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");
    var urlBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
    
    for (let i = 0; i < JSONClientes.value.length; i++) {
        var nuevaFila = document.createElement("tr");
        
        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = JSONClientes.value[i].ContactName;

        var columnaCiudad = document.createElement("td");
        columnaCiudad.innerText = JSONClientes.value[i].City;

        var columnaBandera = document.createElement("td");
        var bandera = document.createElement("img");
        bandera.classList.add("flag");

        if(JSONClientes.value[i].Country == "UK"){
            bandera.src = urlBandera +"United-Kingdom.png";
        }
        else{
            bandera.src = urlBandera+JSONClientes.value[i].Country+".png";
        }

        
        columnaBandera.appendChild (bandera)

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaCiudad);
        nuevaFila.appendChild(columnaBandera);

        tbody.appendChild(nuevaFila);
    }
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}